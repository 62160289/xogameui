/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phankumtong.oxgameui;

/**
 *
 * @author ROG
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;

    private int lastColumn;
    private int lastRow;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;

    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);

            }
            System.out.println("");
        }

    }

    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public boolean setRowCol(int row, int column) {
        if(isFinish()) return false;
        if (table[row][column] == '-') {
            table[row][column] = currentPlayer.getName();
            this.lastRow = row;
            this.lastColumn = column;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getcurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;

        } else {
            currentPlayer = playerX;

        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastColumn] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int column = 0; column < 3; column++) {
            if (table[lastRow][column] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
    }

    void checkX() {
        if (table[0][0] == currentPlayer.getName()) {
            if (table[1][1] == currentPlayer.getName()) {
                if (table[2][2] == currentPlayer.getName()) {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                }
            }
        } else if (table[0][2] == currentPlayer.getName()) {
            if (table[1][1] == currentPlayer.getName()) {
                if (table[2][0] == currentPlayer.getName()) {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();

                }
            }
        }

    }

    void checkDraw() {
        for(int col = 0; col < 3;col++){
            for(int row = 0; row < 3;row++){
                if(table[row][col]=='-'){
                    return;
                }
            }
        }
        finish = true;
        playerO.draw();
        playerX.draw();

    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();

    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;

    }
}
